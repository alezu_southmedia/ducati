var gulp = require('gulp'),
    wiredep = require('wiredep').stream,
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    rimraf = require('gulp-rimraf'),
    uglify = require('gulp-uglify'),
    compass = require('gulp-compass'),
    cssnano = require('gulp-cssnano'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    postcss = require('gulp-postcss'),
    sourcemaps = require('gulp-sourcemaps'),
    removePrefixes = require('postcss-remove-prefixes'),
    autoprefixer = require('gulp-autoprefixer'),
    htmlhint = require('gulp-htmlhint'),
    htmlmin = require('gulp-htmlmin'),
    uncss = require('gulp-uncss'),
    browserSync = require('browser-sync').create(),
    sftp = require('gulp-sftp'),
    rename = require('gulp-rename'),
    hash_src = require('gulp-hash-src'),
    notify = require('gulp-notify');


// SFTP
gulp.task('sftp', ['rn'], function() {
    return gulp.src('dist/**/*')
        // .pipe(sftp({
        //     host: '212.111.43.124',
        //     user: 'root',
        //     pass: 'klst48nt',
        //     remotePath: '/var/www/production/data/www/elsinor-sochi.com/'
        // }))
        .pipe(notify({
            onLast: true,
            message: 'Deploy complete'
        }));
});

gulp.task('sftp_test', ['rn'], function() {
    return gulp.src('dist/**/*')
        .pipe(sftp({
            host: '88.80.186.197',
            user: 'root',
            remotePath: '/var/www/alezu/data/www/ducati.8861.ru/'
        }))
        .pipe(notify({
            onLast: true,
            message: 'Deploy complete'
        }));

});

//clean
gulp.task('clean', function() {
    return gulp.src('dist')
        .pipe(rimraf({
            force: true
        }));
});

// Bower
gulp.task('bower', function() {
    return gulp.src('app/*.html')
        .pipe(wiredep({
            "directory": "app/components"
        }))
        .pipe(gulp.dest('app'))
});

// Image minification
gulp.task('imagemin', function() {
    return gulp.src('app/images/**')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'));
});

gulp.task('autoprefixer', function() {
    return gulp.src('app/css/*.css')
        .pipe(sourcemaps.init())
        // .pipe(postcss([
        //  removePrefixes(),
        //  autoprefixer({ browsers: ['last 15 versions'] })
        //  ]))
        .pipe(autoprefixer('last 10 versions'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('app/css'));
});

// HTML Hinting
gulp.task('htmlhint', function() {
    return gulp
        .src('app/*.html')
        .pipe(htmlhint())
        .pipe(htmlhint.reporter())
});


// HTML Minifying
gulp.task('htmlminify', function() {
    return gulp.src('app/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('dist'))
});

gulp.task('watch', function() {
    browserSync.init({
        server: "./app"
    });
    gulp.watch('./bower.json', ['bower'])
    gulp.watch('./app/sass/*.scss', ['compass'])
        // gulp.watch('./app/css/style.css', ['autoprefixer'])
    gulp.watch('./app/*.html', ['htmlhint']).on('change', browserSync.reload)
});

// UnCSS
gulp.task('uncss', function() {
    return gulp.src('app/css/*.css')
        .pipe(uncss({
            html: ['app/index.html']
        }))
        .pipe(gulp.dest('app/css'));
});

// Copying PHP scripts
gulp.task('php', function() {
    return gulp.src(['app/php/**/*']).pipe(gulp.dest('dist/php'));
});
// Copying Docs
gulp.task('docs', function() {
    return gulp.src(['app/documents/**/*']).pipe(gulp.dest('dist/documents'));
});
// Copying Fonts
gulp.task('fonts', function() {
    return gulp.src(['app/fonts/**/*']).pipe(gulp.dest('dist/fonts'));
});

// Rename to .php
gulp.task('rn', function() {
    return gulp.src(['dist/index.html']).pipe(rimraf()).pipe(rename('index.php')).pipe(gulp.dest('dist'));

});

// Build
gulp.task('dist', ['clean', 'imagemin', 'php', 'docs', 'fonts'], function() {
    return gulp
        .src('app/*.html')
        .pipe(useref())
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', cssnano()))
        .pipe(hash_src({build_dir: "dist", src_path: "dist"}))
        .pipe(gulp.dest('dist'))
        .pipe(notify({
            onLast: true,
            message: 'Build complete'
        }));
});

// Compass
gulp.task('compass', function() {
    gulp.src('./app/sass/*.scss')
        .pipe(compass({
            config_file: './app/config.rb',
            css: 'app/css',
            sass: 'app/sass'
        }))
        .pipe(autoprefixer('last 5 versions'))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.stream());
});

gulp.task('init', ['compass', 'bower']);
