<?php define ('FILE_CACHE_DIRECTORY', '../images/cache');             // Directory where images are cached. Left blank it will use the system temporary directory (which is better for security)
if(! isset($ALLOWED_SITES)){
        $ALLOWED_SITES = array (
                        'flickr.com',
                        'picasa.com',
                        'img.youtube.com',
                        'upload.wikimedia.org',
                        'photobucket.com',
                        'imgur.com',
                        'imageshack.us',
                        'tinypic.com',
						'bizzthemes.com'
        );
}