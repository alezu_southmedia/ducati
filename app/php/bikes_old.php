<?php

global $bikes;

$bikes = array();

$bikes['monster'] = array();
$bikes['monster']['brochure'] = 'MONSTER.pdf';


$bikes['monster']['Monster 821'] = array(
	'volume' => 821,
	'power' => 112,
	'turns' => 9250,
	'price' => 11740,
	'image' => 'Monster_821.jpg',
	'pdf' => 'Monster 821.pdf',
	'is_available' => true,
);
$bikes['monster']['Monster 821 dark'] = array(
	'volume' => 821,
	'power' => 112,
	'turns' => 9250,
	'price' => 10890,
	'image' => 'MONSTER_821_Dark.jpg',
	'pdf' => 'Monster 821 Dark.pdf',
	'is_available' => true,
);
$bikes['monster']['Monster 821 stripe'] = array(
	'volume' => 821,
	'power' => 112,
	'turns' => 9250,
	'price' => 12290,
	'image' => 'MONSTER-821-STRIPE.jpg',
	'pdf' => 'Monster 821 Stripe.pdf',
	'is_available' => true,
);
$bikes['monster']['Monster 1200'] = array(
	'volume' => 1198.4,
	'power' => 135,
	'turns' => 8750,
	'price' => 15490,
	'image' => 'MONSTER1200.jpg',
	'pdf' => 'Monster 1200.pdf',
	'is_available' => true,
);
$bikes['monster']['Monster 1200 S'] = array(
	'volume' => 1198.4,
	'power' => 145,
	'turns' => 8750,
	'price' => 18890,
	'image' => 'MONSTER1200S.jpg',
	'pdf' => 'Monster 1200 S.pdf',
	'is_available' => true,
);
$bikes['monster']['Monster 1200 R'] = array(
	'volume' => 1198.4,
	'power' => 160,
	'turns' => 9250,
	'price' => 22290,
	'image' => 'HYPERMOTARD.jpg',
	'pdf' => NULL,
	'is_available' => true,
);

$bikes['hypermotard']['brochure'] = 'HYPERMOTARD.pdf';
$bikes['hypermotard']['Hypermotard 821'] = array(
	'volume' => 821,
	'power' => 110,
	'turns' => 9250,
	'price' => 12390,
	'image' => 'HYPERMOTARD.jpg',
	'pdf' => 'Hypermotard.pdf',
	'is_available' => true,
);
$bikes['hypermotard']['Hypermotard 939 New'] = array(
	'volume' => 939,
	'power' => 110,
	'turns' => 9250,
	'price' => 13790,
	'image' => 'Hyperstrada-939_red_01_1067x600.jpg',
	'pdf' => 'Hypermotard.pdf',
	'is_available' => true,
);
$bikes['hypermotard']['Hypermotard 939 SP New'] = array(
	'volume' => 939,
	'power' => 110,
	'turns' => 9250,
	'price' => 17790,
	'image' => 'Hypermotard-SP.jpg',
	'pdf' => 'Hypermotard SP.pdf',
	'is_available' => true,
);
$bikes['hypermotard']['Hypermotard 939'] = array(
	'volume' => 937,
	'power' => 113,
	'turns' => 9000,
	'price' => 15090,
	'image' => 'Hyperstrada-939_red_01_1067x600.jpg',
	'pdf' => 'Hypermotard.pdf',
	'is_available' => true,
);

$bikes['multistrada']['brochure'] = 'MULTISTRADA.pdf';
$bikes['multistrada']['Multistrada 1200'] = array(
	'volume' => 1198,
	'power' => 150,
	'turns' => 9250,
	'price' => 21554,
	'image' => NULL,
	'pdf' => 'Multistrada 1200.pdf',
	'is_available' => true,
);
$bikes['multistrada']['Multistrada 1200 Touring Package'] = array(
	'volume' => 1198,
	'power' => 150,
	'turns' => 9250,
	'price' => 21490,
	'image' => 'Multistrada_1200_-Enduro_Touring_Pack.jpg',
	'pdf' => NULL,
	'is_available' => true,
);
$bikes['multistrada']['Multistrada 1200 S'] = array(
	'volume' => 1198,
	'power' => 160,
	'turns' => 9250,
	'price' => 22790,
	'image' => 'Multistrada-1200s-red.jpg',
	'pdf' => 'Multistrada 1200 S.pdf',
	'is_available' => true,
);
$bikes['multistrada']['Multistrada 1200 S Touring Package'] = array(
	'volume' => 1198,
	'power' => 150,
	'turns' => 9250,
	'price' => 23990,
	'image' => NULL,
	'pdf' => 'Multistrada 1200 S.pdf',
	'is_available' => true,
);
$bikes['multistrada']['Multistrada 1200 S Pikes Peak New'] = array(
	'volume' => 1198,
	'power' => 150,
	'turns' => 9250,
	'price' => 27390,
	'image' => NULL,
	'pdf' => NULL,
	'is_available' => true,
);
$bikes['multistrada']['Multistrada 1200 Enduro New'] = array(
	'volume' => 1198,
	'power' => 160,
	'turns' => 9500,
	'price' => 24090,
	'image' => NULL,
	'pdf' => NULL,
	'is_available' => true,
);
$bikes['multistrada']['Multistrada 1200 Enduro Touring Pack'] = array(
	'volume' => 1198,
	'power' => 150,
	'turns' => 9250,
	'price' => 25490,
	'image' => 'Multistrada_1200_-Enduro_Touring_Pack.jpg',
	'pdf' => NULL,
	'is_available' => true,
);



$bikes['scrambler']['brochure'] = 'SCRAMBLER.pdf';
$bikes['scrambler']['Scrambler Classic'] = array(
	'volume' => 803,
	'power' => 75,
	'turns' => 8250,
	'price' => 10990,
	'image' => 'ScramblerClassic.jpg',
	'pdf' => 'Scrambler Classic.pdf',
	'is_available' => true,
);
$bikes['scrambler']['Scrambler Icon Yellow'] = array(
	'volume' => 803,
	'power' => 75,
	'turns' => 8250,
	'price' => 9590,
	'image' => 'SCRAMBLER-ICON.jpg',
	'pdf' => 'Scrambler Icon.pdf',
	'is_available' => true,
);
$bikes['scrambler']['Scrambler Icon Red'] = array(
	'volume' => 803,
	'power' => 75,
	'turns' => 8250,
	'price' => 9190,
	'image' => 'SCRAMBLER-ICON-RED.jpg',
	'pdf' => 'Scrambler Icon.pdf',
	'is_available' => false,
);
$bikes['scrambler']['Scrambler Icon Enduro'] = array(
	'volume' => 803,
	'power' => 75,
	'turns' => 8250,
	'price' => 10990,
	'image' => 'SCRAMBLER-URBAN-ENDURO.jpg',
	'pdf' => 'Scrambler Icon.pdf',
	'is_available' => false,
);
$bikes['scrambler']['Scrambler Full Throttle'] = array(
	'volume' => 803,
	'power' => 75,
	'turns' => 8250,
	'price' => 10990,
	'image' => 'Scrambler-Full-Throttle3.jpg',
	'pdf' => NULL,
	'is_available' => true,
);
$bikes['scrambler']['Scrambler Italia Independent'] = array(
	'volume' => 803,
	'power' => 75,
	'turns' => 8250,
	'price' => 13390,
	'image' => 'scrambler_italia_independent.jpg',
	'pdf' => 'Scrambler Italia Independent.pdf',
	'is_available' => true,
);
$bikes['scrambler']['Scrambler Flat Track Pro'] = array(
	'volume' => 803,
	'power' => 75,
	'turns' => 8250,
	'price' => 11890,
	'image' => 'scrambler_flat_track_pro.jpg',
	'pdf' => 'Scrambler Flat Track Pro.pdf',
	'is_available' => true,
);


$bikes['streetfighter']['brochure'] = 'STREETFIGHTER.pdf';
$bikes['streetfighter']['Streetfighter Fighter 848'] = array(
	'volume' => 849,
	'power' => 132,
	'turns' => 10000,
	'price' => 11632,
	'image' => 'STREETFIGHTER848.jpg',
	'pdf' => 'Streetfighter 848.pdf',
	'is_available' => true,
);


$bikes['diavel']['brochure'] = 'DIAVEL.pdf';
$bikes['diavel']['Diavel Dark'] = array(
	'volume' => 1198,
	'power' => 162,
	'turns' => 9500,
	'price' => 20590,
	'image' => NULL,
	'pdf' => 'Diavel.pdf',
	'is_available' => true,
);
$bikes['diavel']['Diavel Carbon'] = array(
	'volume' => 1198,
	'power' => 162,
	'turns' => 9500,
	'price' => 26690,
	'image' => 'DIAVEL-CARBON.jpg',
	'pdf' => 'Diavel Carbon.pdf',
	'is_available' => false,
);
$bikes['diavel']['XDiavel New'] = array(
	'volume' => 1262,
	'power' => 156,
	'turns' => 9500,
	'price' => 22390,
	'image' => 'XDiavel.jpg',
	'pdf' => 'XDiavel.pdf',
	'is_available' => true,
);
$bikes['diavel']['XDiavel S'] = array(
	'volume' => 1262,
	'power' => 156,
	'turns' => 9500,
	'price' => 26190,
	'image' => 'XDiavel-S.jpg',
	'pdf' => 'XDiavel S.pdf',
	'is_available' => false,
);


$bikes['SBK']['1299 Panigale'] = array(
	'volume' => 1285,
	'power' => 205,
	'turns' => 10500,
	'price' => 26390,
	'image' => 'Panigale-1299.jpg',
	'pdf' => NULL,
	'is_available' => true,
);
$bikes['SBK']['1299 Panigale S'] = array(
	'volume' => 1285,
	'power' => 205,
	'turns' => 10500,
	'price' => 31890,
	'image' => 'Panigale_S_1299.jpg',
	'pdf' => '1299 Panigale S.pdf',
	'is_available' => false,
);
$bikes['SBK']['1299 Panigale R'] = array(
	'volume' => 1198,
	'power' => 205,
	'turns' => 11500,
	'price' => 41290,
	'image' => NULL,
	'pdf' => 'Panigale R.pdf',
	'is_available' => false,
);
$bikes['SBK']['899 Panigale Red'] = array(
	'volume' => 898,
	'power' => 148,
	'turns' => 10750,
	'price' => 0,
	'image' => 'Panigale-899.jpg',
	'pdf' => NULL,
	'is_available' => false,
);
$bikes['SBK']['899 Panigale White'] = array(
	'volume' => 898,
	'power' => 148,
	'turns' => 10750,
	'price' => 0,
	'image' => 'Panigale-white-899.jpg',
	'pdf' => NULL,
	'is_available' => false,
);
$bikes['SBK']['959 Panigale New'] = array(
	'volume' => 898,
	'power' => 148,
	'turns' => 10750,
	'price' => 17790,
	'image' => NULL,
	'pdf' => NULL,
	'is_available' => false,
);






if(isset($_POST['model']) && isset($_POST['group'])) {
	$group = $_POST['group'];
	$model = $_POST['model'];
	$bike = $bikes[$group][$model];
	$availability = $bike['is_available']?'<div class="is_available">В наличии</div>':'';
	$price = $bike['price']?'<div class="price">' . number_format($bike['price'],0,'',' ') . '</div>':'';
	$link = $bike['pdf']?'<a href="/documents/leaflets/' .$bike['pdf'] . '" class="file-download" target="_blank">Технические характеристики</a>':'';
	$big_image = $bikes[$group][$model]['image']?$bikes[$group][$model]['image']:'bike-example.png';
	$output = <<<EOD
	    <a href="#" class="back"></a>
	    <div class="pic"><img src="images/bikes/big/{$big_image}" alt=""></div>
	    <div class="info">
	        {$availability}
	        <div class="brand">Ducati</div>
	        <div class="model">{$model}</div>
	        <div class="params">
	            <div class="param">
	                <span class="param-name">Рабочий объем</span>
	                <span class="param-value">{$bikes[$group][$model]['volume']} см<sup>3</sup></span>
	            </div>
	            <div class="param">
	                <span class="param-name">Мощность</span>
	                <span class="param-value">{$bikes[$group][$model]['power']} л.с. <span>при {$bikes[$group][$model]['turns']} об/мин</span></span>
	            </div>
	        </div>
	        <div class="center">
	            {$price}
	            <a href="#modal-form-buy" class="btn popupper">Купить</a>
	            {$link}
	        </div>
	    </div>
EOD;
	print $output;
}

function model_menu($bikes){
	$groups = array_keys($bikes);
	$output = '';
	foreach($groups as $key => $group) {
		$output .= "<li><a ". (!$key?' class="active" ':'') ."href=\"#\">Серия <span class=\"model-name\">{$group}</span></a></li>";
	}
	return $output;
}

function model_tabs($bikes){
	$groups = array_keys($bikes);
	$output = '';
	foreach($groups as $key => $group) {
		$brochure = array_key_exists('brochure', $bikes[$group])?'<a class="brochure" href="documents/leaflets/groups/' . $bikes[$group]['brochure'] . '" target="_blank">Скачать брошюру</a>':'';
		$output .= '<div class="model-tab'.((!$key)?' active':'').'"><div class="tab-content">';
		foreach($bikes[$group] as $name => $bike){
			if($name == 'brochure') continue;
			if(!$bike['image']) $bike['image'] = 'bike-example.jpg';
			$bike['price'] = number_format($bike['price'],0,'','.');
			$price_thumb = $bike['price']?'<div class="price">' . $bike['price'] . ' &euro;</div>':'';
			$output .= <<<EOD
			 						<div class="item">
                    <div class="pic">
                        <img src="images/bikes/small/{$bike['image']}" alt="{$name}">
                    </div>
                    <div class="info">
                        <div class="title">
                            <h3>{$name}</h3>
                            {$price_thumb}
                        </div>
                        <div class="description">Рабочий объем {$bike['volume']} см <sup>3</sup>
                            <br>Мощность {$bike['power']} л.с. при {$bike['turns']} об.мин
                        </div>
                  	</div>
									</div>
EOD;
		}
		$output .= '</div>' . $brochure . '</div>';
	}
	return $output;
}