;

$(document).ready(function() {

    $('input[type="tel"], input[name="phone"]').mask('+7 (999) 999-99-99');


    $('.section-offers').on('click', '.item', function() {
        if ($(this).find('.full-text').length) {
            if (!$(this).parents('.bx-wrapper').find('.full-item').length) {
                $(this).parents('.bx-wrapper').append('<div class="full-item" />');
            }
            var content = [];
            content.push($(this).find('h3').length ? $(this).find('h3').clone() : '');
            content.push($(this).find('h4').length ? $(this).find('h4').clone() : '');
            content.push($(this).find('.full-text').length ? $(this).find('.full-text').clone() : '');
            content.push($(this).find('.price').length ? $(this).find('.price').clone() : '');
            var image = $(this).css('background-image');

            $(this).parents('.bx-wrapper').find('.full-item')
                .html(content)
                .append('<a class="btn popupper" href="#modal-form-offer">Записаться</a>')
                .prepend('<a href="#" class="close-btn"></a>')
                .css({ 'background-image': image });

        }
    });

    $('.section-models').on('click', '.tab-content .item', function() {

        if (!$(this).parents('.bx-wrapper').find('.full-item').length) {
            $(this).parents('.bx-wrapper').append('<div class="full-item full-item-bike" />');
        }
        var item = $(this);
        $.ajax({
            type: 'POST',
            url: '/php/bikes.php',
            data: 'model=' + $(this).find('h3').text() + '&group=' + $(this).parents('.container').find('.models-nav .active .model-name').text(),
            success: function(data) {
                item.parents('.bx-wrapper').find('.full-item')
                    .html(data);
                $('.fotorama').fotorama();
            },
            error: function(data) {
                item.parents('.bx-wrapper').find('.full-item')
                    .html(data);
            }
        });


    });

    $('.section-services').on('click', '.item', function() {
        if ($(this).find('.full-text').length) {
            var full = $(this).find('.full-text');
            if (!$(this).parents('.items').find('.full-item').length) {
                $(this).parents('.items').append('<div class="full-item" />');
            }
            var content = [];
            content.push($(this).find('h3').length ? $(this).find('h3').clone() : '');
            content.push($(this).find('.full-text').length ? $(this).find('.full-text').clone() : '');
            var image = $(this).css('background-image');

            $(this).parents('.items').find('.full-item')
                .html(content)
                .append('<a class="btn popupper" href="#modal-form-offer">Записаться</a>')
                .prepend('<a href="#" class="close-btn"></a>')
                .css({ 'background-image': image });

        }
    });

    $('.section-slider .items').bxSlider({
        controls: false,
    });

    $('.model-tab').each(function() {
        var $set = $(this).find('.tab-content').children();
        for (var i = 0, len = $set.length; i < len; i += 6) {
            $set.slice(i, i + 6).wrapAll('<div class="items"/>');
        }
    });

    var $set = $('.slider-content').children();
    for (var i = 0, len = $set.length; i < len; i += 4) {
        $set.slice(i, i + 4).wrapAll('<div class="items panels"/>');
    }


    config = {
        onSliderLoad: function(e) {
            if (this.parents('.bx-wrapper').find('.bx-pager .bx-pager-item').length > 1) {
                this.parents('.bx-wrapper').find('.bx-pager').prepend('<div class="pager-prev" />').append('<div class="pager-next" />');
            } else {
                this.parents('.bx-wrapper').find('.bx-pager').hide();
            }
        }
    };

    $(document).on('click', '.full-item .close-btn, .full-item .back', function() {
        $(this).parents('.full-item').fadeOut('slow');
        setTimeout(function() {
            $('.full-item').remove();
        }, 800);
        return false;
    });


    $('.model-tab').first().find('.tab-content').bxSlider(config);

    $('.slider-content').bxSlider(config);

    $(document).on('click', '.pager-prev', function() {
        $(this).parents('.bx-wrapper').find('.bx-prev').trigger('click');
    });

    $(document).on('click', '.pager-next', function() {
        $(this).parents('.bx-wrapper').find('.bx-next').trigger('click');
    });

    $('.models-nav').on('click', 'a', function() {
        if (!$(this).hasClass('active')) {
            var order = $(this).parent('li').index();
            $(this).parents('.models-nav').find('.active').removeClass('active');
            $(this).addClass('active');
            $(this).parents('.container').find('.model-tab.active').removeClass('active');
            $(this).parents('.container').find('.model-tab').eq(order).addClass('active');
            slider = $(this).parents('.container').find('.model-tab').eq(order).find('.tab-content');
            slider.bxSlider(config);
        }
        return false;
    });


    $('section').scrollSpy();


    /* Меню */
    $('nav a').on('click', function() {
        var position = $(this).position().left + $(this).width() / 2 - $('nav .arrow').width() / 2;
        $('nav .arrow').css({ left: position });
        return false;
    });
    /* //Меню */



    /* Модалки */
    $(document).on('click', '.popupper', function() {
        var target = $(this).attr('href');

        if (typeof(target) !== 'undefined' && $(target).length) {
            $('.modal-showed').removeClass('modal-showed');
            $(target).addClass('modal-showed');
            $('.overlay').addClass('showed');
        }
        return false;
    });

    $('.modal').on('click touchstart', '.close-btn', function() {
        $(this).parents('.modal').removeClass('modal-showed');
        $('.overlay').removeClass('showed');
        return false;
    });

    $('.overlay').on('click touchstart', function(event) {
        if ($(event.target).closest('.modal').length == 0) {
            $('.modal-showed').removeClass('modal-showed');
            $('.overlay').removeClass('showed');
            return false;
        }

    });
    /* // Модалки */


    $('.colorbox').colorbox({
        scalePhotos: true,
        maxWidth: '80%',
        maxHeight: '80%',
        onOpen: function() {
            $("#colorbox").addClass("gallery-box");
        },
        onClosed: function() {
            $("#colorbox").removeClass("gallery-box");
        }
    });


    $('form').on('submit', function() {
        send($(this), $(this).parent('div'));
        return false;
    });

}); // End $(document).ready();


function send(el, container) {
    var inputs = el.find('input[type="text"],input[type="tel"],input[type="email"],input[type="hidden"],textarea'),
        data;
    if (container.parents('.modal-content').find('h2').length) {
        data = 'type=' + container.parents('.modal-content').find('h2').text();
    } else if (container.parent('div').find('h3').length) {
        data = 'type=' + container.find('h3').text();
    } else { data = 'type=Заказать звонок'; }
    data += '&id=' + el.attr('id');
    for (var i = 0; i < $(inputs).length; i++) {
        switch ($(inputs[i]).attr('type')) {
            case 'text':
                if ($(inputs[i]).attr('placeholder') == "First name") break;
                if (!$(inputs[i]).val().length) {
                    $(inputs[i]).addClass('error');
                    add_error($(inputs[i]));
                } else {
                    $(inputs[i]).removeClass('error');
                    remove_error($(inputs[i]));
                }
                data += '&' + $(inputs[i]).attr('placeholder') + '=' + $(inputs[i]).val();
                break;
            case 'hidden':
                if ($(inputs[i]).attr('placeholder') == "First name") break;
                data += '&' + $(inputs[i]).attr('placeholder') + '=' + $(inputs[i]).val();
                break;
            case 'tel':
                var phone = $(inputs[i]);

                if ($(phone).val() != '') {
                    var pattern_phone = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{10,12}$/i;
                    if (pattern_phone.test($(phone).val())) {
                        $(phone).removeClass('error');
                        remove_error($(inputs[i]));
                    } else {
                        $(phone).addClass('error');
                        add_error($(inputs[i]));
                    }
                }
                if ($(phone).val() == 0) {
                    $(phone).addClass('error');
                    add_error($(inputs[i]));
                } else {
                    $(phone).removeClass('error');
                    remove_error($(inputs[i]));
                }
                data += '&' + $(inputs[i]).attr('placeholder') + '=' + $(inputs[i]).val();
                break;
            case 'email':
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                if (!filter.test($(inputs[i]).val())) {
                    $(inputs[i]).addClass('error');
                    add_error($(inputs[i]));
                } else {
                    $(inputs[i]).removeClass('error');
                    remove_error($(inputs[i]));
                }
                data += '&' + $(inputs[i]).attr('placeholder') + '=' + $(inputs[i]).val();
                break;
            default:
                if (!$(inputs[i]).val().length) {
                    $(inputs[i]).addClass('error');
                    add_error($(inputs[i]));
                } else {
                    $(inputs[i]).removeClass('error');
                    remove_error($(inputs[i]));
                }
                data += '&' + $(inputs[i]).attr('placeholder') + '=' + $(inputs[i]).val();
                break;

        }
    }

    if ($(el).find('.error').length > 0) {
        console.log('not valid');
    } else {
        $.ajax({
            type: "post",
            url: "php/send.php",
            data: data,
            success: function(msg) {
                var form_id = getFormId(data);
                $('#' + form_id).parents('.modal').removeClass('modal-showed');
                if(!$('#' + form_id).parents('.modal').length) {
                    $('#' + form_id).html('<div class="success">Спасибо, Ваша заявка отправлена. <br />Мы перезвоним в течение суток</div>');
                } else {
                    $('.modal-success').addClass('modal-showed');
                }
                switch (form_id) {
                    case 'form-prices':
                        /*yaEvent('form');
                        yaEvent('form0');*/
                        break;
                    default:
                        //yaEvent('form5');
                        //yaEvent('form0');
                        break;

                }
            },
            error: function(msg) {
                var form_id = getFormId(data);
                $('#' + form_id).parents('.modal').removeClass('modal-showed');
                console.log(form_id);
                if(!$('#' + form_id).parents('.modal').length) {
                    $('#' + form_id).html('<div class="success">Спасибо, Ваша заявка отправлена. <br />Мы перезвоним в течение суток</div>');
                } else {
                    $('.modal-success').addClass('modal-showed');
                }
            },
        });
    }
}

$(window).scroll(function(){
    if (!((navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/Android/i)) || (navigator.userAgent.match(/droid/i)))) {

        $('div[data-type="background"]').each(function() {
            var bgobj = $(this), // создаем объект
                destination = $(this).offset().top,
                topOfWindow = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop,
                heightWindow = $(window).height(),
                defaultCoords = bgobj.css('background-position');

            if (topOfWindow > destination - heightWindow) {
                var yPos = -((topOfWindow - destination) / bgobj.data('speed')); // вычисляем коэффициент
                // Присваиваем значение background-position
                var coords = "calc(50% + 315px)" + ' ' + yPos + 'px';
                // Создаем эффект Parallax Scrolling
                bgobj.css({ backgroundPosition: coords });
            }
        });
    }
});

$(window).resize(function() {

});


/* Functions */

function getFormId(data) {
    data = data.split('&');
    for (var key in data) {
        var element = data[key].split('=');
        if (element[0] == 'id') {
            return element[1];
        }
    }
}


function number_format(number, decimals, dec_point, thousands_sep) {

    var i, j, kw, kd, km;
    if (isNaN(decimals = Math.abs(decimals))) {
        decimals = 2;
    }
    if (dec_point == undefined) {
        dec_point = ",";
    }
    if (thousands_sep == undefined) {
        thousands_sep = ".";
    }
    i = parseInt(number = (+number || 0).toFixed(decimals)) + "";
    if ((j = i.length) > 3) {
        j = j % 3;
    } else {
        j = 0;
    }
    km = (j ? i.substr(0, j) + thousands_sep : "");
    kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
    //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
    kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");
    return km + kw + kd;
}

function add_error(input) {
    remove_error(input);
    if (typeof(input.attr('placeholder') !== 'undefined') && input.attr('placeholder')) {
        var txt = input.attr('placeholder');
        if (txt == 'Имя и фамилия') {
            var error_message = '<div class="error-message"><div class="icon"></div>' + txt + ' не введены</div>';
        } else {
            var error_message = '<div class="error-message"><div class="icon"></div>' + txt + ' введен некорректно</div>';
        }
        //var error_message = '<div class="error-message"><div class="icon"></div> введено некорректно</div>';
    } else {
        var error_message = '<div class="error-message"><div class="icon"></div>Поле введено некорректно</div>';
    }
    $(input).after(error_message);
}

function remove_error(input) {
    input.next('.error-message').remove();
}

function yaEvent(a) {
    if (window['yaCounter33804069']) {
        yaCounter33804069.reachGoal(a);
        console.log(a + ' event has been sent...')
    }
}

$.fn.isOnScreen = function() {
    var element = this.get(0);
    var bounds = element.getBoundingClientRect();
    return bounds.top < window.innerHeight && bounds.bottom > 0;
}

$(document).keyup(function(e) {
    if (e.keyCode == 27) {
        $('.modal-showed').removeClass('modal-showed');
        $('.overlay').fadeOut('slow');
    }
});


function arrowSlide() {
    var link = $('nav a.active');
    if (link.length) {
        var position = link.position().left + link.width() / 2 - $('nav .arrow').width() / 2;
        $('nav .arrow').css({ left: position });
    }
}
