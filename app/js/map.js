var point = new google.maps.LatLng(45.020051, 39.124885);
var center = new google.maps.LatLng(45.020309,39.123529);

function initialize() {
    //var map;
    var infowindow;

    var stylez = [{
        featureType: "all",
        elementType: "all",
        stylers: [{
            //saturation: -100
        }]
    }];

    var mapOptions = {
        zoom: 17,
        center: center,
        mapTypeControlOptions: {
            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'tehgrayz']
        },
        scaleControl: false,
        zoomControl: true,
        scrollwheel: false,
    };

    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    var mapType = new google.maps.StyledMapType(stylez, {
        name: "Ducati"
    });

    var homeMarker = new google.maps.Marker({
        position: point,
        map: map,
        title: 'Центр продаж мотоциклов Ducati',
        icon: {
            url: 'images/icon-placemarker.png',
            size: new google.maps.Size(58, 62),
            anchor: new google.maps.Point(29, 62),
        },
    });

    infowindow = new google.maps.InfoWindow();

}


google.maps.event.addDomListener(window, 'load', initialize);

